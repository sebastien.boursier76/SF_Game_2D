﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class StageManager : MonoBehaviour {

    public static StageManager instance = null;
    public List<StageZone> stageZones = new List<StageZone>();
    public List<GameObject> capsules = new List<GameObject>();

    public int enemyDead;
    public bool lastEnemy;
    public StageZone actualStage;
    public TextMeshProUGUI actualZone;

    private int _stageNumber;
    private int _stageNumberText;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }        
    }

    void Start ()
    {
        // Orders the GameManager to check elements at its launch
        GameManager.instance.CheckElements();
        // Actual stage is the first stage
        actualStage = stageZones[_stageNumber];
    }

    public void ChangeStage()
    {
        // Destroy script of the actual stage
        Destroy(actualStage);
        // Update to the next stage
        ++_stageNumber;
        actualStage = stageZones[_stageNumber];
    }

    public void SpawnCapsule(Transform lastEnemyPos)
    {
        // Select randomly a capsule
        int capsuleNumber = Random.Range(0, capsules.Count);
        // Instantiate the capsule where is located the last enemy
        GameObject capsule = Instantiate(capsules[capsuleNumber], lastEnemyPos);
        // Detache the capsule to enemy in the hierarchy
        capsule.transform.parent = null;
    }

    public void SetZoneText()
    {
        // Update the stage text
        _stageNumberText = _stageNumber + 1;
        actualZone.text = "Zone " + _stageNumberText.ToString();
    }
}
