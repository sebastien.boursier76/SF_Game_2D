﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour {

    public float healthEnemy;
    public float damageEnemy;
    public float healthDiff = 15;
    public float damageDiff = 0.5f;
    public float speedDiff = 0.5f;

    private bool _fading;

    void Start ()
    {
        float navSpeed = GetComponent<NavMeshAgent>().speed;

        // If game is set to medium...
        if (GameManager.instance.difficultSelect == "Medium")
        {
            // ... enemy life, damage and speed increase
            healthEnemy = healthEnemy + healthDiff;
            damageEnemy = damageEnemy + damageDiff;
            navSpeed = navSpeed + speedDiff;
            GetComponent<NavMeshAgent>().speed = navSpeed;
        }
        // If game is set to hard...
        else if (GameManager.instance.difficultSelect == "Hard")
        {
            // ... enemy life, damage and speed increase twice as much
            healthEnemy = healthEnemy + (healthDiff * 2);
            damageEnemy = damageEnemy + (damageDiff * 2);
            navSpeed = navSpeed + (speedDiff * 2);
            GetComponent<NavMeshAgent>().speed = navSpeed;
        }

        // Find the player and follow him
        GetComponent<Follower>().Target = GameObject.FindGameObjectWithTag("Player").transform;
	}	

    void OnCollisionEnter(Collision collision)
    {
        var player = collision.gameObject.GetComponent<PlayerController>();

        // If enemy collides with player...
        if (collision.gameObject.CompareTag("Player"))
        {
            // ... player takes damage
            player.TakeDamage(damageEnemy);
        }
    }    

    public void TakeDamage (float amount)
    {
        // Enemy loses damage indicated
        healthEnemy -= amount;

        // If enemy has no life point...
        if (healthEnemy <= 0f)
        {
            // ... call the function die
            Die();
        }
    }

    void Die()
    {
        // If enemy is the last enemy on actual stage...
        if (StageManager.instance.lastEnemy == true)
        {
            // ... call the function spawn capsule
            StageManager.instance.SpawnCapsule(transform);
        }

        Destroy(gameObject);
        // Add count to enemy dead in actual stage
        ++StageManager.instance.enemyDead;
    }
}
