﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    public static GameManager instance = null;
    public string difficultSelect = "Medium";
    public bool animZoneFinish;
    public bool isDead;
    public GameObject player;
    public GameObject[] enemyPrefab;
    public GameObject gameOverUI;
    public Transform camPos;
    public Image bloodScreen;
    
    private float _fadeSpeed = 1.5f;
    private bool _restart;

    void Awake ()
    {
		if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
	}

    void Update()
    {
        // Check if restart is activated
        if (_restart)
        {
            // If player press "R"...
            if (Input.GetKeyDown(KeyCode.R))
            {
                // ... put timeScale back...
                Time.timeScale = 1;
                // ... and reload the level at the beginning
                SceneManager.LoadScene("Main");
            }
        }
    }

    public void CheckElements()
    {
        // Check if the elements are assigned
        if (player == null)
        {
            player = GameObject.Find("Player");
            camPos = GameObject.Find("CameraHolder").transform;
            bloodScreen = GameObject.Find("BloodDamage").GetComponent<Image>();
            gameOverUI = GameObject.Find("GameOverZone");
            gameOverUI.SetActive(false);
        }

        // Check if the Game Over screen are actived
        if (gameOverUI.gameObject.activeInHierarchy == true)
        {
            gameOverUI.SetActive(false);
        }
    }

    public IEnumerator SpawnerRoutine(int enemyInStage, int spawnIndex, int enemyDifferent, int enemyPrefabIndex, Transform[] spawnPoint)
    {
            // Compare between enemies spawned and enemies have to spawn
            while (StageManager.instance.actualStage.enemyNumber < enemyInStage)
            {
                // Select randomly the enemy prefab
                enemyPrefabIndex = Random.Range(0, enemyDifferent);
                // Select randomly one of spawn predefined
                spawnIndex = Random.Range(0, spawnPoint.Length);
                // Instantiate enemy
                Instantiate(enemyPrefab[enemyPrefabIndex], spawnPoint[spawnIndex].position, spawnPoint[spawnIndex].rotation);
                // Add count to enemies spawned
                ++StageManager.instance.actualStage.enemyNumber;
                yield return new WaitForSeconds(1.5f);
            }
    }

    public IEnumerator BloodEffect()
    {
        // Set the screen
        bloodScreen.color = new Color(1, 0, 0, 125/255f);

        // If the screen alpha is more than 0...
        while (bloodScreen.color.a > 0)
        {
            // ... fade the blood screen
            bloodScreen.color = Color.Lerp(bloodScreen.color, Color.clear, _fadeSpeed * Time.deltaTime);
            yield return null;
        }
    }

    public IEnumerator AnimationDeactivateDoor()
    {
        // Freeze the player
        player.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
        // Detach the camera to the player
        animZoneFinish = true;
        yield return new WaitForSeconds(1f);
        // Set the camera above the doors
        camPos.position = StageManager.instance.actualStage.viewOnDoor.position;
        yield return new WaitForSeconds(1.5f);
        // Call the function to deactivate the doors
        StageManager.instance.actualStage.DeactivateDoor();
        yield return new WaitForSeconds(1f);
        // Reattach the camera to the player
        animZoneFinish = false;
        // Release the player
        player.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
    }

    public IEnumerator GameOver()
    {
        yield return new WaitForSeconds(0.15f);
        // Enable the Game Over screen
        gameOverUI.SetActive(true);
        // Stop the game
        Time.timeScale = 0;
        // Enable the restart
        _restart = true;
    }
}
