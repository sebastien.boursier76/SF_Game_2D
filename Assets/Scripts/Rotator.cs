﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour {
    
	void Update ()
    {
        // Rotate the capsule above the ground
        transform.Rotate(new Vector3(30, 20, 40) * Time.deltaTime);
	}
}
