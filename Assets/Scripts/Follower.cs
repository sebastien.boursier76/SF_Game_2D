﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Follower : MonoBehaviour {

    public Transform Target;
    public NavMeshAgent Agent;
    	
	void Awake ()
    {
        Agent = GetComponent<NavMeshAgent>();
	}
	
	void Update ()
    {
        // Set enemy to follow player position
        Agent.SetDestination(Target.position);
	}
}
