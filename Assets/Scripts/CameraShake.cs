﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShake : MonoBehaviour {

	public IEnumerator Shake(float duration, float magnitude)
    {
        // Set to the actual camera position
        Vector3 originalPos = transform.localPosition;

        float elapsed = 0.0f;
        // While the duration isn't elapsed...
        while (elapsed < duration)
        {
            // ... select randomly a position...
            float x = Random.Range(-0.5f, 0.5f) * magnitude;
            float y = Random.Range(-0.5f, 0.5f) * magnitude;
            // ... set the new position...
            transform.localPosition = new Vector3(x, y, originalPos.z);
            // ... and add time elapsed
            elapsed += Time.deltaTime;

            yield return null;
        }
        // Put camera back to the original position
        transform.localPosition = originalPos;
    }
}
