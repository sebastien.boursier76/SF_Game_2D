﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class ButtonManager : MonoBehaviour {

    public GameObject mainMenu;
    public GameObject optionsMenu;
    public GameObject creditsScreen;
    public TextMeshProUGUI difficultText;
    
	public void LoadScene(string newGameLevel)
    {
        SceneManager.LoadScene(newGameLevel);
    }

    public void BrowseSubmenu(string submenu)
    {
        // If player select back to the main menu...
        if (submenu =="MainMenu")
        {
            // ... deactivate options menu or credits screen...
            optionsMenu.gameObject.SetActive(false);
            creditsScreen.gameObject.SetActive(false);
            // ... and activate main menu
            mainMenu.gameObject.SetActive(true);
        }
        // If player select options menu...
        else if (submenu == "Options")
        {
            // ... deactivate main menu...
            mainMenu.gameObject.SetActive(false);
            // ... and activate options menu
            optionsMenu.gameObject.SetActive(true);
        }
        // If player select credits screen...
        else if(submenu == "Credits")
        {
            // ... deactivate main menu...
            mainMenu.gameObject.SetActive(false);
            // ... and activate credits screen
            creditsScreen.gameObject.SetActive(true);
        }
    }

    public void ChangeDifficult()
    {
        // If game is set to easy...
        if (GameManager.instance.difficultSelect == "Easy")
        {
            // ... change difficult to medium
            GameManager.instance.difficultSelect = "Medium";
            difficultText.text = "Medium";
        }
        // If game is set to medium...
        else if (GameManager.instance.difficultSelect == "Medium")
        {
            // ... change difficult to hard
            GameManager.instance.difficultSelect = "Hard";
            difficultText.text = "Hard";
        }
        // If game is set to hard...
        else if (GameManager.instance.difficultSelect == "Hard")
        {
            // ... change difficult to easy
            GameManager.instance.difficultSelect = "Easy";
            difficultText.text = "Easy";
        }
    }
}
