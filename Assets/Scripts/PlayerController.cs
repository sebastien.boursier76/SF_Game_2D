﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

    public float speed = 6f;

    public float lifePlayer = 100;
    public AudioClip hurtSound;
    public LayerMask floorMask;
    public Image lifeBar;

    private float _startLife;
    private bool _restoreLife;
    private Vector3 _movement;
    private Rigidbody _rb;

    
    void Start ()
    {
        _rb = GetComponent<Rigidbody>();
        _startLife = lifePlayer;
	}
	
	void FixedUpdate ()
    {
        // Update the life bar according to the life player
        lifeBar.fillAmount = (float)lifePlayer / _startLife;

        // Player movement with the horizontal and vertical keys
        float h = Input.GetAxisRaw("Horizontal") * speed * Time.deltaTime;
        float v = Input.GetAxisRaw("Vertical") * speed * Time.deltaTime;

        _rb.velocity = new Vector3(h, 0f, v).normalized * speed;

        // Check the mouse position
        Turning();
	}

    void OnDrawGizmos()
    {        
        Gizmos.DrawSphere(Camera.main.ViewportToWorldPoint(Input.mousePosition), 1);
    }


    void Turning()
    {
        // Create a ray from the mouse cursor on screen in the direction of the camera.
        Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition);

        // Create a RaycastHit variable to store information about what was hit by the ray.
        RaycastHit floorHit;

        // Perform the raycast and if it hits something on the floor layer...
        if (Physics.Raycast(camRay, out floorHit, Mathf.Infinity, floorMask))
        {
            // Create a vector from the player to the point on the floor the raycast from the mouse hit.
            Vector3 playerToMouse = floorHit.point - transform.position;

            // Ensure the vector is entirely along the floor plane.
            playerToMouse.y = 0f;

            // Create a quaternion (rotation) based on looking down the vector from the player to the mouse.
            Quaternion newRotation = Quaternion.LookRotation(playerToMouse);

            // Set the player's rotation to this new rotation.
            _rb.MoveRotation(newRotation);
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        // If player collides with enemy...
        if (collision.gameObject.CompareTag("Enemy"))
        {
            // ... activate the blood screen effect
            StartCoroutine(GameManager.instance.BloodEffect());
        }
        // If player collides with capsule...
        else if (collision.gameObject.CompareTag("Capsule"))
        {
            // ... destroy capsule
            Destroy(collision.gameObject);

            // If there is a next stage...
            if (StageManager.instance.actualStage.nextZone == true)
            {
                // ... start the animation to deactivate the doors
                StartCoroutine(GameManager.instance.AnimationDeactivateDoor());
            }
        }
    }

    public void TakeDamage(float amount)
    {
        // Player loses damage indicated
        lifePlayer -= amount;

        SoundManager.instance.PlaySound(hurtSound);

        // If player has no life point...
        if (lifePlayer <= 0f)
        {
            // ... start the game over
            StartCoroutine(GameManager.instance.GameOver());
        }

        // If the life restoration is disabled...
        if (_restoreLife == false)
        {
            // ... activate this
            StartCoroutine(RestoreLife());
            _restoreLife = true;
        }
    }

    public IEnumerator RestoreLife()
    {
        while (lifePlayer < 100)
        {
            yield return new WaitForSeconds(1.5f);
            // Player recoves 2.5 life points
            lifePlayer += 2.5f;
            
            // If player has all life points or more...
            if (lifePlayer >= 100)
            {
                // ... set life points to 100...
                lifePlayer = 100;
                // ... disable the life restoration
                _restoreLife = false;
            }
        }        
    }
}
