﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour {

    public float damage = 20f;
    public float range = 100f;

    public AudioClip laserSound;
    public ParticleSystem flashLaser;
    public CameraShake cameraShake;

    float _rateOfFire = 0.5f;
    float _fireDelay;
    Vector3 _lastHitPos;

    void Update ()
    {
        // If player press the button fire 1...
		if (Input.GetButtonDown("Fire1") && Time.time > _fireDelay)
        {
            // ... delay between fires is update...
            _fireDelay = Time.time + _rateOfFire;
            // ... call the function shoot...
            Shoot();
            // ... and call the function camera shake
            StartCoroutine(cameraShake.Shake(0.15f, 0.05f));
        }
    }

    void Shoot()
    {
        // Call the function to play a flash laser
        flashLaser.Play();
        SoundManager.instance.PlaySound(laserSound);

        RaycastHit hit;

        if (Physics.Raycast(transform.position, transform.forward, out hit, range))
        {
            Enemy enemy = hit.transform.GetComponent<Enemy>();
            _lastHitPos = hit.transform.position;

            // If enemy has a script Enemy...
            if (enemy != null)
            {
                // ...inflict damage to the enemy
                enemy.TakeDamage(damage);
            }
        }
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(_lastHitPos, 0.1f);
    }
}
