﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class StageZone : MonoBehaviour {

    public int enemyInStage;
    public int enemyNumber;
    public int spawnIndex;
    public int enemyPrefabIndex;
    public int enemyDifferent;
    public bool nextZone = true;
    public Transform[] spawnPoint;
    public Transform viewOnDoor;
    public GameObject[] doorStart;
    public GameObject[] doorFinish;

    void Update()
    {
        // Check if it's the last enemy
        if(StageManager.instance.enemyDead == (enemyInStage - 1))
        {
            StageManager.instance.lastEnemy = true;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        // If player go into the stage...
        if (other.gameObject.CompareTag("Player"))
        {
            // ... activate doors of the new stage...
            StartCoroutine(ActivateDoor());
            // ... update the zone text...
            StageManager.instance.SetZoneText();
            // ... and activate the spawn
            StartCoroutine(GameManager.instance.SpawnerRoutine(enemyInStage, spawnIndex, enemyDifferent, enemyPrefabIndex, spawnPoint));
        }
    }

    void OnTriggerExit(Collider other)
    {
        // If player go out the stage...
        if (other.gameObject.CompareTag("Player"))
        {
            // ... activate doors of the last stage...
            doorFinish.ToList().ForEach(p => p.SetActive(true));
            // ... and update the actual stage
            StageManager.instance.ChangeStage();
        }
    }

    IEnumerator ActivateDoor()
    {
        // Activate doors
        yield return new WaitForSeconds(0.5f);
        doorStart.ToList().ForEach(p => p.SetActive(true));
    }

    public void DeactivateDoor()
    {
        // Set by default the last enemy and enemy dead number...
        StageManager.instance.lastEnemy = false;
        StageManager.instance.enemyDead = 0;
        // ... and deactivate doors
        doorFinish.ToList().ForEach(p => p.SetActive(false));
    }
}
